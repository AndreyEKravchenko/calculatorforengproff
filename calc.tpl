/*
*	Created by Andrey Kravchenko
*	riderak@gmail.com
*/
<link href="catalog/view/theme/default/template/popup/css/calc.css" rel="stylesheet">
<div id="form-<?php echo $pre_id; ?>" class="fixed">
	<div class="close-form"></div>
	<div class="cell" style="width:820px;">
		<div class="form">
			<div class="close">
				<svg viewBox="0 0 16 16" width="10" height="10"><path d="M9.3 8l6.7 6.7-1.3 1.3L8 9.3 1.3 16 0 14.7 6.7 8 0 1.3 1.3 0 8 6.7 14.7 0 16 1.3 9.3 8z"></path></svg>
			</div>		
			<div class="title">Калькулятор расчета цилиндров EngeneerProff</div>
			<div class="scroll">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-12">
						<span>
							<p class="smalltitle">
								Цилиндры произведенные нашей компанией EngeneerProff упаковываются в картонную коробку.
							</p>
						</span>

						<div class="content_separator">
    					</div>
    					<div class="mainblock">
							<span class="smalltitle1"> Расчёт количества коробок, веса и объема заказа</span>
							<div class="div_one"> 					
									<div class="form-group required">
									<label class="control-label"><?php echo "Диаметр изолируемой поверхности (мм)" ?>
									</label>
									<input type="number" name="date" id="diametr_tube" value="10" min="10" step="1" class="form-control">
								</div>

								<div>
									<img class="img1" src="/image/calc/boxes1.jpg" alt="коробка">
									<span class="field1">
										Количество коробок
									</span>
									<span class="out_1" id="out1">0</span>
									<span class="out_2">шт.</span>

								</div>

								<div class="form-group form1 required">
									<label class="control-label"><?php echo "Толщина изоляции (мм)" ?></label>
									<input type="number" name="insulation" id="insulation_thickness" value="20" min="20" step="1" class="form-control">
								</div>

								<div>
									<img class="img2" src="/image/calc/volume.png" alt="коробка">
									<span class="field2">
										Объем
									</span>
									<span class="out_3" id="volume">0</span>
									<span class="out_4">м<sup><small>3</small></sup></span>
								</div>

								<div class="form-group form1 required">
									<label class="control-label"><?php echo "Общая длина (метры)" ?></label>
									<input type="number" name="long_text" id = "long" value="500" min="1" class="form-control">
									<div class="form-group col-md-12">
	      								<input type="range" class="long-range" min="10" max="5000" step="1" name="longr" id="long_range" value="500"/>
	    							</div>
								</div>
								<div style="height: 0px;">
									<img class="img_3" src="/image/calc/weight.png" alt="коробка">
									<span class = "field3">
										Вес
									</span>
									<span class="out_5" id="out_weight1">0</span> 
									<span class="out_6">кг.</span>
								</div>
								
								<div style="height: 0px;">
									<img class="img_4" src="/image/calc/box_our.jpg" alt="коробка">
								</div>

								<div class="form-group form2">
									<label class="control-label">Плотность&nbspкг/м<sup><small>3</small></sup></label>
										<p><select class="select_style" size="1" name="plotnost" id = "density">
    									<option selected value="80">80</option>
    									<option value="100">100</option>
    									<option value="120">120</option>
    									<option value="150">150</option>
   									</select></p>
									
								</div>
								<div style="height: 0px;">
									<img class="img_5" src="/image/calc/plotnost.png" alt="engenproff">
									<span class = "field4">
										Плотность
									</span>
									<span class="out_7" id="density_show">80</span>
									<span class="out_8">&nbspкг/м<sup><small>3</small></sup></span>
									
								</div>
							
							</div>
						</div>
					</div>					
				</div>	
			</div>
			<div class="newBlock">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-12">
							<div class="tunecalc" id="Newform">
								<input value="Добавить рассчет" onclick="newFormArray()" type="button">
								<input class="deleteForm" value="Удалить рассчет" onclick="deleteLastForm()" type="button" disabled>
							</div>
						</div>
					</div>
				</div>		
			</div>

			<div>
				<span class="finaly">Итого:</span>
			<div>
				<div class="container">
					<div class="row">			
						<div class="col-lg-6 col-md-12">
							<div class="form4">
								<div class="form5">
									<span>Итого количество коробок&nbsp </span><span id="out">0</span> <span class="rub">шт.</span>
									<div class="col-lg4">
										<span>Общий вес:&nbsp </span><span id="out_weight">0</span><span class="rub">кг.</span>
									</div>
									<div>
										<span> Объем&nbsp</span><span id="volume_finish">0</span>
										<span >м<sup><small>3</small></sup></span>
									</div>
										<span> Плотность&nbsp</span><span id="density_finish">80</span>
										<span >кг/м<sup><small>3</small></sup></span>
									<div>

									</div>
									
			    					<div class="sendtomail" id="DIV">
			          					<div class="form-group required">
											<label class="control-label" style="margin-top: 15px;"><?php echo $text_email; ?></label>
											<input type="text" name="email" value="<?php echo $customer_email; ?>" class="form-control">
										</div>
										<div class="text-agree" style="margin-top: 15px;">
			            					<?php echo $text_agree; ?>
			          					</div>
										<div class="buttons"><button class="btn orang" style="margin-right: 60px;top: -46px;position: relative;margin-bottom: -29px;">Отправить запрос КП</button></div>
									</div>
	          					</div>
	      					</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script>
var box_x = 0.4, box_y = 0.5, weight1smkv = 80;
var _form = $('body').find('#form-<?php echo $pre_id; ?> .form');

var elem_new = [];

$('body').on('click', '#form-<?php echo $pre_id; ?> .form button.btn', function(){ 	
 	valid = validForm(_form);
	if(!valid)
		return false;

	var data = [];

	data[0] = {			
		name: 'Форма',
		value: _form.find('.title').text()
	};
	
	inputs =  _form.find('.form-group').length;
	
	for(var i = 0; i < inputs; i ++){	
		var key = i +1;		 
		data[key] = {			
			name: _form.find('.form-group').eq(i).find('label').text(),
			value: _form.find('.form-group').eq(i).find('input, textarea').val()
		};
	}
	
	$.ajax({
		url: 'index.php?route=popup/popup/sendForm',
		type: 'post',
		dataType: 'json',
		data: {data: data, page: document.title, url: window.location.href},
		beforeSend: function() {
			_form.find('button.btn').text('<?php echo $text_loading; ?>');
		},
		complete: function() {
			_form.find('button.btn').text('<?php echo $text_send; ?>');
		},
		success: function(json) {
			
			$('body').find('#form-<?php echo $pre_id; ?>').remove();
			
			if (json['success']) {				
				$.get('index.php?route=popup/popup&type=thanks', function(result) {
					$('body').append(result);
				});
			}
			
			if (json['error']) {				
				$.get('index.php?route=popup/popup&type=error', function(result) {
					$('body').append(result);
				});
			}			
		},
        error: function(xhr, ajaxOptions, thrownError) {
            console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			$('body').find('#form-<?php echo $pre_id; ?>').remove();
			$.get('index.php?route=popup/popup&type=error', function(result) {
				$('body').append(result);
			});
        }
	});
});

function kolvo(){
	weight1smkv = $('#density').val();
	radius=($('#diametr_tube').val()/2)/1000 + $('#insulation_thickness').val()/1000;
	diametrcilindra = 2*radius;	
	kol_vo_in_box = Math.floor(box_x/diametrcilindra)*Math.floor(box_y/diametrcilindra);
	if (kol_vo_in_box === Infinity) {
		kol_vo_in_box = 1;
	}
	kol_vo_boxes = parseInt($('#long').val())/kol_vo_in_box;
	if (kol_vo_boxes === Infinity) {
		$.get('index.php?route=popup/popup&type=error_calc', function(result) {
					$('body').append(result);
				});
		kol_vo_boxes = 1;
	}
	console.log(Math.ceil(kol_vo_boxes));
	cylinder_volume = Math.PI*radius*radius*$('#long').val()-Math.PI*($('#diametr_tube').val()/2)/1000*($('#diametr_tube').val()/2)/1000**parseInt($('#long').val());
	console.log(cylinder_volume);
	volume = 0.0;
	volume = Math.ceil(kol_vo_boxes) * 1.03 * box_x * box_y;
	$("#out").text(Math.ceil(kol_vo_boxes));
	$("#out1").text(Math.ceil(kol_vo_boxes));
	$("#volume").text(volume.toFixed(2));
	weight = cylinder_volume*weight1smkv
	$("#out_weight").text(weight.toFixed(2));
	$("#out_weight1").text(weight.toFixed(2));
	$("#volume_finish").text(volume.toFixed(2));


	document.getElementById("DIV").style.display = "block";
}

$("#density").change(function(){
     density= $('#density').val();
     $("#density_show").text(density);
     $("#density_finish").text(density);
	kolvo();	
});


$("#diametr_tube").change(function(){
    weight = $('#diametr_tube').val();
    kolvo();
});


$("#insulation_thickness").change(function(){
    weight = $('#insulation_thickness').val();
    kolvo();
});

$("#long").change(function(){
    weight = $('#long').val();
    $("#long_range").val(weight);
    kolvo();
});


$("#long_range").change(function(){
	weight = $('#long_range').val();
    $("#long").val(weight);
    kolvo();
});


$('body').on('click', '#form-<?php echo $pre_id; ?> .close-form, #form-<?php echo $pre_id; ?> .close', function(){
	$('body').find('#form-<?php echo $pre_id; ?>').remove();
	$('body').css({overflow: ''});
});


/*Добавить расчет*/
	

function newFormArray() {
	var doc = document; // закешировал дом дерево
	document.getElementsByClassName("deleteForm").disabled = false; // кнопке удалить атрибут enabled

	let Label1 = [];
	let Label1_content = [];
	let input1 = [];
	 
	var elem = doc.createElement("div");
	elem.getElementsByClassName("new_form").id="iddd";
	elem_new.push(elem);
	console.log(elem_new);
	var newformP = doc.getElementById("Newform");

	elem.className='new_form';
	newformP.parentNode.insertBefore(elem,newformP);  //разместил главный div для формы

	var close_span  = doc.createElement("span");
	close_span.className='delete_section';
	elem.appendChild(close_span);

	Label1_content[0] = doc.createTextNode("Диаметр изолируемой поверхности (мм)");
	Label1_content[1] = doc.createTextNode("Толщина изоляции (мм)");
	Label1_content[2] = doc.createTextNode("Общая длина (метры)");
	var i=0;
	while (i < 3) {
		Label1[i] = doc.createElement("label");
		input1[i] = doc.createElement("input");

		Label1[i].className='control-label';
		input1[i].className='form-control';				

		Label1[i].appendChild(Label1_content[i]);	
		elem.appendChild(Label1[i]);
		elem.appendChild(input1[i]);
		i++;
	}
}

function newForm() {
	var doc = document; // закешировал дом дерево

	document.getElementsByClassName("deleteForm").disabled = false; 

	var elem = doc.createElement("div"),
		input_diametr_tube = document.createElement("input"),
	/*	content = doc.createTextNode("Диаметр изолируемой поверхности (мм)"),*/
		newformP = doc.getElementById("Newform");

	elem.className='new_form';
	/*elem.appendChild(content);*/

	newformP.parentNode.insertBefore(elem,newformP);  //разместил главный div для формы

	var label_diametr_tube = doc.createElement("label"),
		label_content = doc.createTextNode("Диаметр изолируемой поверхности (мм)");

	/*newformP.parentNode.insertBefore(label_diametr_tube, newformP);*/
	label_diametr_tube.appendChild(label_content);
	elem.appendChild(label_diametr_tube);

	var input_diametr_tube = doc.createElement("input");
	input_diametr_tube.className='form-control';
	elem.appendChild(input_diametr_tube);


	var label_insulation_thickness = doc.createElement("label"),
		label_insulation_thickness_content = doc.createTextNode("Толщина изоляции (мм)");

	label_insulation_thickness.appendChild(label_insulation_thickness_content);
	elem.appendChild(label_insulation_thickness);

	var input_insulation_thickness = doc.createElement("input");
	input_insulation_thickness.className='form-control';
	elem.appendChild(input_insulation_thickness);


	var label_common_long = doc.createElement("label"),
		label_common_long_content = doc.createTextNode("Обшая длина (метры)");

	label_common_long.appendChild(label_common_long_content);
	elem.appendChild(label_common_long);

	var input_common_long = doc.createElement("input");
	input_common_long.className='form-control';
	elem.appendChild(input_common_long);

}

/*удаляем последнюю добавленную расчет*/
function deleteLastForm(){
	alert('удаляем последний!');
}

</script>
